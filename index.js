import express from 'express';

const app = express();

app.listen(80, () => {
    console.log('Server on port 80');
});

app.get('/', (req, res) => {
    res.json('Hello World 111');
});